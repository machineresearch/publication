READ ME

‘From Page Rank to Rank Brain’ is an essay that attempts to ‘decloak’ as well as ‘update’ public knowledge about Google a.k.a. Alphabet’s ranking algorithm. This text has then been altered through 3 ‘translation’ processes. 

Drawing on Constant’s collection of scripts,1 the first translation used ‘encryption_lines_sha1.py’ that ‘provides the ultimate reduction (although at the expense of human as well as machine legibility) by encrypting every line of your text as a 128-bit hash value. Each hash value can of course be reversed again if you try to match it with every single line of every single text existing.’2 The second translation uses a little python script called the ‘The Synonymizer’ that corrupts your writing style by swapping out words in your text with randomized synonyms from WordNet.3 With the third translation, the text was first read with the ’text to speech’ voice of ‘Alex’ and saved as an audio file, then uploaded to ’gentle’, a robust yet lenient ‘forced aligner’ built on Kaldi.4 Forced aligners are computer programs that take media files and their transcripts and return extremely precise timing information for each word (and phoneme) in the media. How does it work? “As in all of these Machine Learning cases, you have to follow the data.”5



1.https://gitlab.constantvzw.org/machineresearch/reduction/tree/master/filters
2.In cryptography, SHA-1 (Secure Hash Algorithm 1) is a cryptographic hash function designed by the United States National Security Agency and is a U.S. Federal Information Processing Standard published by the United States NIST in 1993. SHA-1 produces a 160-bit (20-byte) hash value known as a message digest. A SHA-1 hash value is typically rendered as a hexadecimal number, 40 digits long.
3.”Note:it may also corrupt the meaning of your text which replaces ‘choice words’ with synomyms.” WordNet: http://wordnet.princeton.edu/. Thanks 2 Dave Young
4.http://lowerquality.com/gentle/ 
5.In this case, it's the CALLHOME corpus, which is 120 unscripted 30-minute telephone conversations between native speakers of English in the 1990s.https://catalog.ldc.upenn.edu/LDC97S42. Thanks 2 Robert M. Ochshorn
